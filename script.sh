column="1"
nlines=$(./a.out -q /home/alx/d/gfxsrc/clements_edit.png | awk -e "{print \$$column}" | uniq | wc -l)
nkeep=$(python2 -c "print ($nlines/38)+1")

./a.out -q /home/alx/d/gfxsrc/clements_edit.png | awk -e "{print \$$column}" | uniq | awk -e "NR == 1 || NR % $nkeep == 0" - | grep . - | sed -e 's/\(.*\):\(.*\)/\2/' - | tr '\n' ',' | sed -e 's/,/, /g' -
echo ""
