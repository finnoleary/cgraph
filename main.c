#define STB_IMAGE_IMPLEMENTATION
#define STBI_ASSERT(x)
#include "stb_image.h"

/* None of this is particularly 'best' practice.
   This is more of a quick hack than anything else. */

struct options {
	int quietp;
	char *path;
	int startx, endx, starty, endy, stepx, stepy;
};

struct image {
	unsigned char *image;
	int width, height, bpp, n;
};

const char *helpstr = "graph [-q] image.png startx:endx starty:endy stepx:stepy\n"
                      "Defaults are:\n"
		      "\tstartx:endx = 0:<image width>\n"
		      "\tstarty:endy = 0:<image height>\n"
		      "\tstepx:stepy = 1:1\n"
		      "\nPass -q to shut it up and just output the data\n";

void split(char *s, int *a, int *b)
{
	char *sp = s;

	while (*sp && *sp != ':')
		sp++;

	if (*sp != ':') {
		printf("bad character in argument: '%c'\n", *sp);
		exit(1);
	}
	else {
		*sp++ = '\0';
	}

	*a = atoi(s);
	*b = atoi(sp);
}


void procarg(struct options *args, char **argv)
{

	int i = 0;
	if ((strcmp(argv[1], "-q")) == 0) { args->quietp = 1; i++; }

	args->path = argv[i+1];

	if (!argv[i+2]) return;
	split(argv[i+2], &(args->startx), &(args->endx));

	if (!argv[i+3]) return;
	split(argv[i+3], &(args->starty), &(args->endy));

	if (!argv[i+4]) return;
	split(argv[i+4], &(args->stepx), &(args->stepy));
}


unsigned char *pos(struct image *img, int x, int y)
{
	int offset = (y * img->width + x)*img->n;
	if (x < 0 || x > img->width || y < 0 || y > img->height) {return NULL;}
	return img->image + offset;
}


int lightness(int r, int g, int b)
{
	return (int)floor((r+g+b)/3);
}


int main(int argc, char **argv)
{
	struct options args = {0};
	struct image img = {0};
	int *lines = NULL;

	if (argc < 2) { printf("%s", helpstr); return 0; }
	args.stepx = args.stepy = 1; procarg(&args, argv);

	if (!args.quietp) {
		printf("arguments: %s %d:%d; %d:%d; %d:%d;\n", args.path,
		       args.startx, args.endx,
		       args.starty, args.endy,
		       args.stepx, args.stepy);
	}

	/* R,G,B output */
	if (!(img.image = stbi_load(args.path, &(img.width), &(img.height), &(img.bpp), (img.n = 3)))) {
		printf("Catastrophic error while loading provided file\n");
		exit(1);
	}

	if (!args.endx || args.endx > img.width) {
		if (!args.quietp) { printf("endx is larger than width. Constraining.\n"); }
		args.endx = img.width;
	}
	if (!args.endy || args.endy > img.height) {
		if (!args.quietp) { printf("endy is larger than height. Constraining.\n"); }
		args.endy = img.height;
	}

	if (!args.startx) args.startx = 1;
	if (!args.starty) args.starty = 1;

	unsigned char *p;
	int x, y, l; /* x, y, lightness */
	int cs, ce;  /* consecutive start, end */
	for (x = args.startx; x > 0 && x < args.endx; x += args.stepx) {
		for (y = args.starty; y > 0 && y < args.endy; y += args.stepy) {
			p = pos(&img, x, y);
			l = lightness(p[0], p[1], p[2]);
			if (l < 128) {
				if (!cs) { cs = y; }
			}
			else if (l > 128 && cs) {
				ce = y-1;
				int ny = (cs == ce) ? cs : abs(ce-((ce-cs)/2));
				printf("%d:%d\t", x, ny);
				cs = ce = 0;
			}
		}
		printf("\n");
	}

	return 0;
}
